import java.util.Arrays;
class MyIterator <E>
{
    E[][] mas;
    Integer positionX;
    Integer positionY;
    MyIterator(E[][] mas)
    {
        this.mas = mas;
        positionX=0;
        positionY=0;
    }

    E next()
    {
        if(hasNext())
        {
            if (positionX == mas[positionY].length - 1)
            {
                positionX = 0;
                positionY++;
            }
            else
            {
                positionX++;
            }
        }
        return mas[positionY][positionX];

    }

    E first()
    {
        return mas[0][0];
    }

    E last()
    {
        return mas[mas.length-1][mas[mas.length-1].length-1];
    }

    E now()
    {
        return mas[positionY][positionX];
    }

    void show()
    {
        System.out.println(Arrays.deepToString(mas));
    }

    Boolean hasNext()
    {
        if(positionX == mas[positionY].length-1&&positionY == mas.length-1) return false;
        else return true;
    }

    void remove()
    {
        Integer i=0;
        Boolean flag = false;
        E[] temp=mas[positionY].clone();
        while(i< temp.length)
        {
            if(!flag)
            {
                if (i == temp.length-1)
                {
                    temp[i]=null;
                }
                else
                {
                    if (now() == temp[i]) {
                        temp[i] = temp[i + 1];
                        flag = true;
                    }
                }
            }
            else
            {
                if (i == temp.length-1)
                {
                    temp[i]=null;
                }
                else
                {
                    temp[i] = temp[i + 1];
                }
            }
            i++;
        }
        mas[positionY] = Arrays.copyOfRange(temp,0,temp.length-1);
    }
}